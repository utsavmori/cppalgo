#include<stdio.h>
#include<stdlib.h>
// using namespace std;

// First subarray is arr[p..q] 
// Second subarray is arr[q+1..r]
void merge(int arr[],int p,int q,int r){
	int i,j,k;
	int n1=q-p+1;
	int n2=r-q;
	int L[n1],R[n2];
	/* Copy data to temp arrays L[] and R[] */
	for(i=0;i<n1;i++){
		L[i]=arr[p+i];
	}
	for(j=0;j<n2;j++){
		R[j]=arr[q+1+j];
	}
	/* Merge the temp arrays back into arr[p..r]*/
	i = 0; // Initial index of first subarray 
    j = 0; // Initial index of second subarray 
    k = p; // Initial index of merged subarray
    while(i<n1 && j<n2){
    	if(L[i] <=R[j]){
    		arr[k]=L[i];
    		i++;
    	}else{
    		arr[k]=R[j];
    		j++;
    	}
    	k++;
    } 

		// if size is not equal	

     /* Copy the remaining elements of L[], if there 
       are any */
    while (i < n1) 
    { 
        arr[k] = L[i]; 
        i++; 
        k++; 
    } 
  
    /* Copy the remaining elements of R[], if there 
       are any */
    while (j < n2) 
    { 
        arr[k] = R[j]; 
        j++; 
        k++; 
    } 

}

void merge_sort(int arr[],int start_index,int end_index){
	if(start_index<end_index){
		int middle_index = start_index + (end_index-start_index)/2;
		merge_sort(arr,start_index,middle_index);
		merge_sort(arr,middle_index+1,end_index);
		merge(arr,start_index,middle_index,end_index);
	}

}

void printArray(int A[], int size){
	int i; 
    for (i=0; i < size; i++) 
        printf("%d ", A[i]); 
    printf("\n"); 
}

int main(){
	int arr[] = {12, 11, 13, 5, 6, 7}; 
    int arr_size = sizeof(arr)/sizeof(arr[0]); 
  
    printf("Given array is \n"); 
    printArray(arr, arr_size); 
  
    merge_sort(arr, 0, arr_size - 1); 
  
    printf("\nSorted array is \n"); 
    printArray(arr, arr_size); 
    
	return 0;
}